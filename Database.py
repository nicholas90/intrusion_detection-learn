"""Class Database, si occupa della creazione e della gestione del Database sqlite.

Nel database vi sono [3 o più] tabelle:
    - la prima tabella indica il nome dei file .pcap che ho splittato in flussi bidirezionali ed il link da cui l'ho
    scaricato, i quale non si trovano più nella directory del progetto
    (nel caso in cui i file pcap non sono stati presi da internet, il campo link risulterà [null o none])
    - la seconda tabella riporta la directory ed i nomi dei file .pcap contenenti flussi bidirezionali di cui ho calcolato
    le label (ma da cui non ho ancora estratto le Features)
    - la terza tabella riporta i nomi dei file .pcap contenenti flussi bidirezionali a cui ho attribuito una label,
    estratto le features e memorizzato nel Dataset il relativo vettore contenente le features
    - la quarta tabella riporta la directory contenente i nomi dei file .pcap contenenti flussi bidirezionali
    dei quali non ho calcolato le label (ma da cui non ho ancora estratto le Features)

Nel Database verranno memorizzati, per ogni flusso, la directory, ed il nome del file .pcap rappresentato da quel flusso.

 (nel quale verranno memorizzate le informazioni sulle quer).

L'oggetto Parser si occupa dell'interfaccia a riga di comando.
Prende i parametri da inseriti dall'utente da terminale, i quali, saranno memorizzati in args ed indicano:
sia quali sono i moduli che vuole chiamare, sia qual'è lordine nel quale devono essere chiamati.
La lista args viene trasformata in tuple, e dei mediante dei metodi, vengono eseguiti una serie di test:
- controlla se la lunghezza di ogni tupla è giusta
- se i parametri passati sono presenti realmente nella cartella.
Una volta acquisiti questi dati, trasformati in tuple e controllata la loro veridicità, vengono passati all'oggetto Orchestrator.

Args:
    con:
    tableFlowLabel: è una stringa contenente il nome della tabella con memorizzati i flussi e le label che gli ho attribuito

"""
import sqlite3 as lite


class Database(object):

    def __init__(self):
        self.con = None
        self.tableLabel = ''


    def get_version_of_the_SQLite_database(self):

        import sys

        try:
            #con = sqlite3.connect('test.db')
            con = lite.connect('test.db')

            cur = con.cursor()
            cur.execute('SELECT SQLITE_VERSION()')

            data = cur.fetchone()

            print("SQLite version: %s" % data)

        except (lite.Error):

            print("Error %s:" % lite.Error.args[0])
            sys.exit(1)

        finally:

            if con:
                con.close()

    """Valuta se la lunghezza delle tuple è quella che ci si aspetta.

    Ad esempio: 
    Se inseriamo il parametro -l, ci aspettiamo che sia seguita da due path (ossia quello del file pcap e quello con il file
    che contiene le label). Se non è così, l'esecuzione termina.

    Args:
        Tuples: Lista di tuple.
        keys: A sequence of strings representing the key of each table row
            to fetch.
        other_silly_variable: Another optional variable, that has a much
            longer name than the other args, and which does nothing.

    Returns:
        A dict mapping keys to the corresponding table row data
        fetched. Each row is represented as a tuple of strings. For
        example:

        {'Serak': ('Rigel VII', 'Preparer'),
         'Zim': ('Irk', 'Invader'),
         'Lrrr': ('Omicron Persei 8', 'Emperor')}

        If a key from the keys argument is missing from the dictionary,
        then that row was not found in the table.

    Raises:
        IOError: An error occurred accessing the bigtable.Table object.
    """

    import collections

    def save_flow_with_label(folder, PCAPflow, label):
        Labelled_Flow = collections.namedtuple('Database', 'id folder name_file label')

        print(type(Labelled_Flow))

        # TO DO: id deve prendere il numero dell'ultimo elemento inserito nella tabella
        bo = Labelled_Flow(id=1, folder=folder, name_file=PCAPflow, label=label)


if __name__ == "__main__":

    print(lite.version)
    print(lite.sqlite_version)

    database = Database()

    database.get_version_of_the_SQLite_database()
