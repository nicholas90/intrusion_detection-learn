import os
import sys
from sys import version_info

from Parser import Parser
from Orchestrator import Orchestrator
import Utility

def main():

    print('Start System')  # OR Start Intrusion_Detection-learn for Botnet

    py3 = version_info[0] > 2  # crea un valore booleano per testare che l'interprete Python attivo sia > 2

    if py3:

        os.chmod(os.getcwd(), 0b111111111)

        Utility.check_directories()

        args = sys.argv

        print(args)

        parser = Parser(args)

        tuples_args = parser.execute()

        #exit(0)

        orchestrator = Orchestrator(tuples_args)

        orchestrator.run_orchestrator()

    else:

        print("Stai usando un interprete < di python 3; per ora il codice non è compatibile per quell'interprete.")

        print("Riprova ad eseguire il codice con un'interprete per Python 3.")


if __name__ == "__main__":

    main()
