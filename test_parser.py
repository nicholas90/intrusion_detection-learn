from unittest import TestCase
from Parser import Parser


class TestParser(TestCase):

    def test_parser(self):
        p = Parser()
        p.addArgument_parser(["directory", "test", "-l", "asd", "ads", "-h", "asd", "asdf", "-a", "-b"])
        self.assertEquals(p.parser(), [("test",), ("-l", "asd", "ads"), ("-h", "asd", "asdf"), ("-a",), ("-b",)])

    def test_fail_parser(self):
        p = Parser(["directory", "tet", "-l", "test", "-h"])
        self.assertNotEquals(p.parser(), [("test",), ("-l", "test"), ("-h",)])