
import os
import signal
import subprocess
import shlex
import time

def run_command(command):

    # The os.setsid() is passed in the argument preexec_fn so it's run after the fork() and before  exec() to run the shell.
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)

    return process

def stop_command(process_pid):

    os.killpg(os.getpgid(process_pid), signal.SIGTERM) # Send the signal to all the process groups


if __name__ == "__main__":

    name = "prova"

    process = run_command("tshark -w " + name + ".pcap -F pcap")

    print(process.pid)

    process_pid = process.pid

    time.sleep(25.0)

    stop_command(process_pid)

    # process.kill()

