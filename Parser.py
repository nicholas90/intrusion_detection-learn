from Switch import Switch

"""Class Parser, si occupa della creazione di un Parser.

L'oggetto Parser si occupa dell'interfaccia a riga di comando.
Prende i parametri da inseriti dall'utente da terminale (dove ogni parametro rappresenta una funzionalità mesa a 
dispozione dal programma) i quali, saranno memorizzati in args ed indicano:
sia quali sono le funzionalità che l'utente vuole utilizzare (nel caso in cui vuole utilizzarne più di una) sia qual'è 
l'ordine nel quale devono essere utilizzate (ogni funzionalità sarà seguita dai relativi parametri).
La lista args viene trasformata in tuple, nello specifico ogni tupla avrà la seguente forma:
-[nome funzionalità_1] [parametro_1] [parametro_2] [...] -[nome funzionalità_1] [parametro_1] [...] [etc]. 
Mediante appositi metodi, vengono eseguiti una serie di test:
- verifica se la funzione richiesta viene realmente messa a disposizione
- controlla se la lunghezza di ogni tupla è giusta
- se i parametri passati sono presenti realmente nella cartella.
Eseguite wueste operazioni (quindi una volta acquisiti questi dati, trasformati in tuple e controllata la loro 
veridicità), vengono passati all'oggetto Orchestrator.

Args:
    args: lista contenente le funzionalità (ed i rispettivi parametri) che l'utente può passare al file "main.py", nel 
    momento in cui viene eseguito.
    Le funzionalità (ed i rispettivi parametri), che è possibile passare al Parser, sono:
    -c: per catturare traffico.
        A sua volta il traffico catturato, verrà salvato in un file .pcap situato nella directory '../pcaps' e come nome 
        gli verrà attribuito il timestamp)
    -s: per splittare un file .pcap (seguito dalla directory del file).
    -l: per assegnare label ai file .pcap già splittati in precedenza; 
        Le label che è possibile attribuire sono di tre tipi: 0 traffico normale, 1 traffico malevolo comunicazione tra Bot e C&C, 2 traffico scaturito da attacco
        Per determinare la label verrà prima lanciato wireshark su quel flusso e poi CapTipper.
        Una volta chiusi entrambi i programmi, se viene determinata la label verranno salvati nel database: 
        il path del file .pcap, il nome del file .pcap e la rispettiva label.
        Se per qualche motivo non si è riusciti a determinare la label, è possibile scartare il flusso, attribuendogli la label '-1'
        (in questo caso il flusso verrà spostato nella directory '../discarded_flows')
        Il parametro -l può essere seguito: 
        nel caso in cui vuoi assegnare label a tutti i file presenti nella direcotry, dalla directory dove sono situati i file già splittati, 
        oppure nel caso in cui vuoi assegnare label ad un solo file, deve essere seguito da due parametri cioè la directory ed il nome del file.
    -nl per assegnare label ai file .pcap già splittati in precedenza (ma in questo caso le label sono già note)
    # TO DO vedere se serve implementarlo, e come va implementato
    
"""
# TO DO: gestire parametri args non validi
# TO DO: ge


class Parser(object):

    def __init__(self, args):
        self.args = args

    def __str__(self):
        str(self.args)

    """Setta il parametro args dell'oggetto Parser, assegnadogli la lista args he gli viene passata in input. 
    
    Args:
        args: lista di argomenti. 
        Contiene gli argomenti, passati dall'utente al file "main.py" nel momento in cui viene eseguito.
    """
    def addArgument_parser(self, args):
        self.args = args

    """Converte la lista di argomenti, memorizzata nel parametro args dell'oggetto Parser, in tuples. 

    Ad esempio: 
    Se inseriamo il parametro -l, ci aspettiamo che sia seguita da due path (ossia quello del file pcap e quello con il file che contiene le label).
    Se non è così, l'esecuzione termina.

    Args:
        Tuples: Lista di tuple.

    Returns:
        Una lista di tuple, ogni tupla contiene un valore passato da terminale con i rispettivi parametri.
        Esempio:

        [('-l', 'labelled_pcaps/pcaps/file1.pcap', 'labelled_pcaps/label/file1.pcap.netflow.labeled'), 
         ('-nl',), 
         ('-c',)]
    """
    def convert_arguments_in_tuples(self):

        splitted_args = []
        counter = 1
        while counter < len(self.args):
            print(self.args[counter])
            if self.args[counter][0] == "-":
                next_arg = counter + 1

                # se ci sono argomenti senza "-" li prende
                while next_arg < len(self.args) and self.args[next_arg][0] != "-":
                    next_arg += 1

                # inserisco nella tupla, gli argomenti (cioè il/i carattere/i col -, più il/i suo/suoi parametro/i)
                splitted_args.append(tuple(self.args[counter:next_arg]))
                counter = next_arg

            # se al primo argomento che trova non c'è il "-"
            else:
                splitted_args.append((self.args[counter],))
                counter += 1
        print(splitted_args)
        return splitted_args

    """Valuta la veridicità delle funzionalità richieste dall'utente.
    
    Valuta se le funzionalità richieste dall'utente da terminale sono realmente messe a disposizione dal sistema. 
    TODO: In caso non lo sono, l'esecuzione termina con un messaggio di errore
    """

    def valuate_existence_functionality(self, tuples):

        import Run_Program

        for t in tuples:
            v = t[0]
            #if v in ()

            for case in Switch(v):
                print('v: ', v)
                exit(0)
                if case('-s'):  #
                    exit(0)
                """
                if case('-l'):  # caso in cui si hanno a disposizione sia il file pcap che le label
                    if len(t) != 3:
                        print("Il numero di parametri che hai passato a -nl è errato, devi passargli 3 parametri; riprova!")
                        exit(0)
                """
                if case('-c'):
                    print(10)
                    break
                if case('eleven'):
                    print(11)
                    break
                if case():  # default, could also just omit condition or 'if True'
                    print("something else!")
                    # No need to break here, it'll stop anyway

    """Valuta se la lunghezza delle tuple è quella che ci si aspetta.

    Ad esempio: 
    Se inseriamo il parametro -l, ci aspettiamo che sia seguita da due path (ossia quello del file pcap e quello con il file che contiene le label).
    Se non è così, l'esecuzione termina.

    Args:
        Tuples: Lista di tuple.
        keys: A sequence of strings representing the key of each table row
            to fetch.
        other_silly_variable: Another optional variable, that has a much
            longer name than the other args, and which does nothing.

    Returns:
        A dict mapping keys to the corresponding table row data
        fetched. Each row is represented as a tuple of strings. For
        example:

        {'Serak': ('Rigel VII', 'Preparer'),
         'Zim': ('Irk', 'Invader'),
         'Lrrr': ('Omicron Persei 8', 'Emperor')}

        If a key from the keys argument is missing from the dictionary,
        then that row was not found in the table.

    Raises:
        IOError: An error occurred accessing the bigtable.Table object.
    """
    def valuate_length_of_tuple(self, tuples):

        for t in tuples:
            v = t[0]
            for case in Switch(v):
                print('len(t):', len(t), 'v', v)
                if v == '-nl':
                    print('qai')
                if case('-nl'):  #
                    if len(t) != 2:
                        print('entra in -nl')
                        print("Il numero di parametri che hai passato a -nl è errato, devi passargli 2 parametri; riprova!")
                        exit(0)
                    break
                if case('-a'):  # caso in cui si hanno a disposizione sia il file pcap che le label
                    if len(t) != 3:
                        print('entra in -a')
                        print("Il numero di parametri che hai passato a -nl è errato, devi passargli 3 parametri; riprova!")
                        exit(0)
                if case('-c'):
                    print(10)
                    break
                if case('eleven'):
                    print(11)
                    break
                if case():  # default, could also just omit condition or 'if True'
                    print("something else!")
                    # No need to break here, it'll stop anyway

    """Valuta il numero di parametri, di ogni possibile opzione.
    
    Valuta se il numero di parametri passati ad ogni argomento (da teminale) è quello che ci si aspetta in modo che 
    l'esecuzione possa continuare senza problemi.
    
    Valuta se il numero di parametri delle tuple è quella che ci si aspetta.
    
    
    Ad esempio: 
    Se inseriamo -l, ci aspettiamo che sia seguito da due path (ossia quello del file pcap e quello con il 
    file che contiene le label). Se non è così, l'esecuzione termina.

    Args:
        Tuples: Lista di tuple.
        keys: A sequence of strings representing the key of each table row
            to fetch.
        other_silly_variable: Another optional variable, that has a much
            longer name than the other args, and which does nothing.

    Returns:
        A dict mapping keys to the corresponding table row data
        fetched. Each row is represented as a tuple of strings. For
        example:

        {'Serak': ('Rigel VII', 'Preparer'),
         'Zim': ('Irk', 'Invader'),
         'Lrrr': ('Omicron Persei 8', 'Emperor')}

        If a key from the keys argument is missing from the dictionary,
        then that row was not found in the table.

    Raises:
        IOError: An error occurred accessing the bigtable.Table object.
    """
    def valuate_content_of_tuple(self, tuples):
        for t in tuples:
            v = t[0]
            for case in Switch(v):
                # l
                if case('-nl'):
                    try:
                        open(t[1])
                    except FileNotFoundError:
                        print('Il file', str(t[1]), 'non è presente nella directory di lavoro')
                        exit(0)
                    finally:
                        break
                if case('-l'):
                    try:
                        open(t[1])
                    except FileNotFoundError:
                        print('Il file', str(t[1]), 'non è presente nella directory di lavoro')
                        exit(0)
                    try:
                        open(t[2])
                    except FileNotFoundError:
                        print('Il file %s non è presente nella directory ..', str(t[2]))
                        exit(0)
                if case('ten'):
                    print(10)
                    break
                if case('eleven'):
                    print(11)
                    break
                if case():  # default, could also just omit condition or 'if True'
                    print("something else!")
                    # No need to break here, it'll stop anyway


    def execute(self):

        tuples_args = self.convert_arguments_in_tuples()

        #self.valuate_existence_functionality(tuples_args)

        self.valuate_length_of_tuple(tuples_args)

        self.valuate_content_of_tuple(tuples_args)

        return tuples_args