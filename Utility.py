"""Il File Utility.py contiene una serie di funzioni "utili".

Ad esempio:
- funzioni per lanciare programmi come tshark per la cattura del traffico
- funzioni per la creazione di directory
- funzioni per verificare l'esistenza di directory
- funzioni per salvare oggetti in Collection
- ecc
"""

import collections

"""Valuta se la lunghezza delle tuple è quella che ci si aspetta.

Ad esempio: 
Se inseriamo il parametro -l, ci aspettiamo che sia seguita da due path (ossia quello del file pcap e quello con il file che contiene le label).
Se non è così, l'esecuzione termina.

Args:
    Tuples: Lista di tuple.
    keys: A sequence of strings representing the key of each table row
        to fetch.
    other_silly_variable: Another optional variable, that has a much
        longer name than the other args, and which does nothing.

Returns:
    A dict mapping keys to the corresponding table row data
    fetched. Each row is represented as a tuple of strings. For
    example:

    {'Serak': ('Rigel VII', 'Preparer'),
     'Zim': ('Irk', 'Invader'),
     'Lrrr': ('Omicron Persei 8', 'Emperor')}

    If a key from the keys argument is missing from the dictionary,
    then that row was not found in the table.

Raises:
    IOError: An error occurred accessing the bigtable.Table object.
"""
def save_flow_with_label(folder, PCAPflow, label):
    Labelled_Flow = collections.namedtuple('Database', 'id folder name_file label')

    print(type(Labelled_Flow))

    # TO DO: id deve prendere il numero dell'ultimo elemento inserito nella tabella
    bo = Labelled_Flow(id=1, folder=folder, name_file=PCAPflow, label=label)


"""Verifica l'esistenza delle directory necessarie al funzionamento del progetto

Valuta se tutte le directory necessarie al corretto funzionamento del sistema, sono presenti nella cartella del progetto.
Se non lo sono, vengono create.
L'elenco delle ditectory necesarie si trova nel file "list_directories.txt".
Una volta verificata l'esistenza ed aver creato le directory mancanti, vengono attribuiti alle cartelle, i permessi di lettura e scrittura. 

"""
def check_directories():

    try:
        # legge la lista di directory contenuta nel file list_directories.txt
        list_directories = []
        with open('list_directories.txt', 'r') as directories:
            # print("---")
            data = directories.read()
            # print(data)
            # print(data.split(","))

            for directory in data.split(","):

                print("directory: ", str(directory))

                # print("os.walk(directory): ", str(os.walk(directory)))

                # cwd = os.getcwd()

                # print(cwd)

                if os.path.isdir(directory):
                    os.chmod(directory, 0b111111111)
                else:
                    os.makedirs(directory)
                    os.chmod(directory, 0b111111111)

    # TO DO: rivedere gestione eccezione
    except (OSError, IOError) as e:
        print(e)

#def create_directory(path, nameFolder):

import os
"""Esegue "SplitCap".

SplitCap è un programma ....
serve per la separazione di file.pcap in flussi di traffico, d
Dove per flussi di traffico intendo comunicazioni bidirezionali tra due host

Args:
    Tuples: Lista di tuple.
    keys: A sequence of strings representing the key of each table row
        to fetch.
    other_silly_variable: Another optional variable, that has a much
        longer name than the other args, and which does nothing.

Returns:
    A dict mapping keys to the corresponding table row data
    fetched. Each row is represented as a tuple of strings. For
    example:

    {'Serak': ('Rigel VII', 'Preparer'),
     'Zim': ('Irk', 'Invader'),
     'Lrrr': ('Omicron Persei 8', 'Emperor')}

    If a key from the keys argument is missing from the dictionary,
    then that row was not found in the table.

Raises:
    IOError: An error occurred accessing the bigtable.Table object.
"""
def run_splitcap(path_filePCAP):

    print("path_filePCAP: ", path_filePCAP)
    filePCAP = path_filePCAP.split("/")[-1]

    splitcap_path = 'SplitCap_2-1/SplitCap.exe'# TO DO fare un metodo che lo ritorna

    #print(os.system('ls'))
    pathwithResults = 'flussi_bidirezionali/' + filePCAP
    print(path_filePCAP)
    print("mono %s -r %s -o %s -s hostpair" % (splitcap_path,path_filePCAP,pathwithResults))
    os.system("mono %s -r %s -o %s -s hostpair" % (splitcap_path,path_filePCAP,pathwithResults))
    print("pathwithResults", pathwithResults)
    return pathwithResults


