from Switch import Switch
#from Utility import run_splitcap

"""Classe Orchestrator, si occupa di chiamare i moduli richiesti dall'utente.

Prende in input una lista di tuple (le quali contengono gli argomenti presi da riga di comando), generati mediante la classe Parser, ed in base ai valori
contenuti nelle tuple, chiama il/i modulo/i richiesto/i passandogli i rispettivi parametri.

 chiama i moduli richiesti, in base alle tuple che gli vengono passate.


Attributes:
    likes_spam: A boolean indicating if we like SPAM or not.
    eggs: An integer count of the eggs we have laid.
"""


class Orchestrator(object):

    def __init__(self, tuple_args):
        self.tuple_args = tuple_args

    def __str__(self):
        str(self.tuple_args)

    def run_orchestrator(self):
        """
        TO DO: gestire -h + uscita
        """
        for t in self.tuple_args:
            v = t[0]
            print("v:", v)
            for case in Switch(v):
                if case('-nl'):
                    self.nl_case(t[1])
                    break
                if case('-l'):
                    break
                if case('ten'):
                    print(10)
                    break
                if case('eleven'):
                    print(11)
                    break
                if case():  # default, could also just omit condition or 'if True'
                    print("something else!")
                    # No need to break here, it'll stop anyway

        def nl_case():
            print("t[1]:", t[1])
            path_filePCAP = t[1]
            # commentato per non far risplittare tutto
            # run_splitcap(path_filePCAP)
            path_BidirectionalFlow = "flussi_bidirezionali/botnet-capture-20110818-bot-2.pcap/"
            print(path_BidirectionalFlow)

            # import Create_Dataset
            path_fileLabels = t[2]
            # Create_Dataset.associate_FlowToLabel(path_filePCAP,path_fileLabels)
            print('Pare che ha fatto')

        # -l 'labelled_pcaps/pcaps/botnet-capture-20110818-bot-2.pcap' 'labelled_pcaps/label/capture20110818-2.pcap.netflow.labeled'

